aa='130、131、132、145、155、156、166、171、175、176、185、186、166、133、149、153、173、177、180、181、189、199、134、135、136、137、138、139、147、150、151、152、157、158、159、172、178、182、183、184、187、188、198、170、192'
a=aa.split('、')
def direct_insertion_sort(d):   # 直接插入排序，因为要用到后面的希尔排序，所以转成function
    d1 = [d[0]]
    for i in d[1:]:
        state = 1
        for j in range(len(d1) - 1, -1, -1):
            if i >= d1[j]:
                d1.insert(j + 1, i)  # 将元素插入数组
                state = 0
                break
        if state:
            d1.insert(0, i)
    return d1
print(direct_insertion_sort(a))